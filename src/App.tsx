import { AppBar, Content } from "src/components";
function App() {
  return (
    <div className="App">
      <AppBar />
      <Content />
    </div>
  );
}

export default App;
